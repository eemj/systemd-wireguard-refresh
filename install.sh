#!/bin/bash

cp ./reresolve-dns-systemd.sh /opt/reresolve-dns-systemd.sh
cp ./systemd-wireguard-refresh.service /etc/systemd/system/systemd-wireguard-refresh.service
cp ./systemd-wireguard-refresh.timer /etc/systemd/system/systemd-wireguard-refresh.timer
chmod +x /opt/reresolve-dns-systemd.sh
chmod a+r /etc/systemd/system/systemd-wireguard-refresh.*

systemctl enable --now systemd-wireguard-refresh.timer
